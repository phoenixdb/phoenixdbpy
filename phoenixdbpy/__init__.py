"""
This file is part of phoenixdb.  phoenixdb is free software: you can
redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, version 3.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 51
Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

Copyright (c) 2013-2016, PhoenixDB Team
"""
from connection import Connection
from error import ExecutionError, InternalError
from array import Array
from schema import Schema
from result import Result
from type import *

__version__ = '0.1.0'
