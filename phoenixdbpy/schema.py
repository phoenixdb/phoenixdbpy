"""
This file is part of phoenixdb.  phoenixdb is free software: you can
redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, version 3.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 51
Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

Copyright (c) 2013-2016, PhoenixDB Team
"""

class Attribute(object):
    def __init__(self, att_id, name, type_id, flags):
        """

        :param att_id: Attribute ID
        :param name: Attribute name
        :param flags: Attribute flags 1 - nullable, 2 - empty indicator
        :param type_id: Attribute type
        """
        self._id = att_id
        self._name = name
        self._type_id = type_id
        self._nullable = flags & 1
        self._empty_indicator = flags & 2

    @property
    def id(self):
        """
        Attribute ID

        :rtype : int
        :return: attribute ID
        """
        return self._id

    @property
    def name(self):
        """
        Attribute name

        :rtype : str
        :return: attribute name
        """
        return self._name

    @property
    def type(self):
        """
        Attribute type

        :rtype : str
        :return: attribute type
        """
        return self._type_id

    @property
    def nullable(self):
        return self._nullable

    @property
    def empty_indicator(self):
        return self._empty_indicator

    def __str__(self):
        return self.name + ':' + self.type


class Dimension(object):
    def __init__(self, name, start, end, chunk_interval):
        """
        :param name: Dimension name
        """
        self._name = name
        self._start = start
        self._end = end
        self._chunk_interval = chunk_interval

    @property
    def name(self):
        """
        Dimension name

        :rtype : str
        :return: dimension name
        """
        return self._name

    @property
    def start(self):
        return self._start

    @property
    def end(self):
        return self._end

    @property
    def chunk_interval(self):
        return self._chunk_interval

    def __str__(self):
        return self.name


class Schema(object):
    def __init__(self, array_name, attributes, dimensions):
        """
        :param array_name: array name
        :param attributes:
        :param dimensions:
        """
        self._array_name = array_name
        self._attributes = attributes
        self._dimensions = dimensions
        self._empty_indicator = None

        for attr in self.attributes:
            if attr.empty_indicator:
                self._empty_indicator = attr
                break

    @property
    def array_name(self):
        """
        Array name

        :rtype : str
        :return: array name
        """
        return self._array_name

    @property
    def attributes(self):
        """
        Attributes list

        :rtype : list
        :return: attributes
        """
        return self._attributes

    @property
    def dimensions(self):
        """
        Dimensions list

        :rtype : list
        :return: dimensions
        """
        return self._dimensions

    @property
    def empty_indicator(self):
        return self._empty_indicator

    def __str__(self):
        attrs = ', '.join(str(x) for x in self._attributes)
        dims = ', '.join(str(x) for x in self._dimensions)
        return self._array_name + '<' + attrs  + '>[' + dims + ']'